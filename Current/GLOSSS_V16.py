#GLOSSS (Graphical Lunar Orbital STK Simulation Software) 1.5
#Compatible with Lib_LOSSS_V12.py module
#6/8/23

from Lib_LOSSS_V13 import *

def select(*args): # function corresponding to element radio buttons
    
    bstr = "lb|ub|int" # string for regular expressions to disable range entries

    indstr = "\." + radioelem[int(selected_ind.get())] # string for regular expression to identify independent variable

    for child in mainframe.winfo_children(): # iterate through the widgets in the mainframe
        
        mbd = re.search(bstr,str(child)) # see if child name matches bounds or int
        mind = re.search(indstr,str(child)) # see if child name matches ind var
        if isinstance(child, ttk.Entry) and mbd is not None and mind is None: # if widget is an entry, is a bound or int entry, and is not the ind var
            child.config(state= "disabled") # disable entry if it matches conditions
        elif isinstance(child, ttk.Entry) and mbd is None and mind is not None: # if widget is an entry, is not a bount or int entry, and is the ind var
            child.config(state= "disabled") # diable entry if it matches conditions
        else: # if widget doesn't match either of the above conditions
            child.config(state= "enabled") # enable entry if it doesn't match above conditions
        
def assign(*args): # function to assign variables and put them into a tuple to pass to the main function
    rpassign = float(rpel.get()) if rpel.get() != '' else 0 # radius of perigee: ternary to test whether the single value entry is empty
    eassign = float(eel.get()) if eel.get() != '' else 0 # eccentricity: " "
    iassign = float(iel.get()) if iel.get() != '' else 0 # inclination: " "
    omlassign = float(omlel.get()) if omlel.get() != '' else 0 # argument of perigee " "
    omuassign = float(omuel.get()) if omuel.get() != '' else 0 # right angle of ascending node " "
    nuassign = float(nuel.get()) if nuel.get() != '' else 0 # mean anomaly " "
    newargs = [0,0,0,0,0,0] # define tuple of zeroes to hold element values for passing
    eldict = { # dictionary to hold contents of all elem entries
        '0':[rpel,rplb,rpub,rpint],
        '1':[eel,elb,eub,eint],
        '2':[iel,ilb,iub,iint],
        '3':[omlel,omllb,omlub,omlint],
        '4':[omuel,omulb,omuub,omuint],
        '5':[nuel,nulb,nuub,nuint]
    }
    
    for x in range(6): # iterate through number of elements
         newa = eldict.get(str(x)) # retrieve correct value from dictionary
         newargs[x] = float(newa[0].get()) if newa[0].get() != '' else 0 # ternary to pass the value to the correct index in the tuple unless it's empty
    
    indvar = eldict.get(str(selected_ind.get())) # retrieve the tuple w/ the ind var entries
    for y in range(4): # iterate through contents of tuple
        indvar[y] = float(indvar[y].get()) if indvar[y].get() != '' else 'na' # convert entry vars to floats and store

    newargs[int(selected_ind.get())] = indvar # store ind var values w/ rest of elems
    
    return newargs # return the stored elem values

def run(*args): # function to run the main functionality
    
    newargs = assign() # run the assign function
    stk, stkRoot = initializeStk(str(scenname.get())) #Opens STK and creates scenario named according to scenarioName variable
    scenario = stkRoot.CurrentScenario #Assigns new scenario to scenario variable
    
    scenario.SetTimePeriod('29 Aug 2022 17:00:00.000', '+2 years') # times are UTCG
    stkRoot.Rewind() # reset to beginning
        
    facility = stkRoot.CurrentScenario.Children.NewOnCentralBody(8, "myfac", "moon") #Create facility object (8) on lunar surface
    
    # IAgFacility facility: Facility Object
    facility.Position.AssignGeodetic(-90, 45, 0)  #Assign LLA position (selenographic coordinates) of facility to the south pole at ground level
    
    # Set altitude to height of terrain
    facility.UseTerrain = True #Use lunar terrain to determine height of facility position
    
    # Set altitude to a distance above the ground
    facility.HeightAboveGround = 0.0  # km, Assign facility height to 0
    
    status = 0
    ind = 0

    indvin = newargs[int(selected_ind.get())] # retrieve tuple with independent variable
    indrange = indvin[2] - indvin[1] # establish the range of values to iterate through
    indlen = (indrange / indvin[3]) + 1 # establish number of values to iterate through
    indvec = [0 for x in range(int(indlen))] # establish an empty vector to store values
    for k in range(int(indlen)): indvec[k] = indvin[1] + k*indvin[3] # calculate values to iterate through
    for index in indvec: # iterate through values
        newargsfun = newargs # pass elem values to a holder
        newargsfun[int(selected_ind.get())] = index # pass individual ind var value into tuple in place of tuple
        mainfun(str(scenname.get()),newargsfun,str(masssat.get()),ind,stk,stkRoot,scenario) # run main function

        status = 1
        ind = ind+1
root = Tk() #create main content window
root.title("GLOSSS V1.5") #set main content window title to "GLOSSS VX.X" 

mainframe = ttk.Frame(root,padding="3 3 12 12") #create frame widget to hold contents
mainframe.grid(column=0,row=0,sticky=(N,W,E,S)) #place it in the main window
root.columnconfigure(0,weight=1) #expand frame to fill window if resized
root.rowconfigure(0,weight=1) # "

ttk.Label(mainframe,text="Initial Conditions (km and degrees)").grid(column=2,row=1,sticky=(W,E)) # label column of initial conditions entries

selected_ind = StringVar() # create string variable for radio buttons
radioelem = ('rp','e','i','oml','omu','nu') # create strings corresponding to elements for regular expressions
m = 0
j = 2
# radio buttons
for elem in radioelem: # iterate trhough elements
    r = ttk.Radiobutton( # create radio buttons
        mainframe, # place them in mainframe
        value=m, # set value of variable on iteration
        variable=selected_ind, # assign stringvar to radio buttons
        command = select # name command and match it to radio buttons
    )
    r.grid(column=1,row=j,sticky=(W,E)) # place radio buttons
    j = j + 1 # iterate indices 
    m = m + 1 # "
    
###

rpel = StringVar() # create radius of perilune string vars
rplb = StringVar() # "
rpub = StringVar() # "
rpint = StringVar() # "
eel = StringVar() # create eccentricity string vars
elb = StringVar() # "
eub = StringVar() # "
eint = StringVar() # "
iel = StringVar() # create inclination string vars
ilb = StringVar() # "
iub = StringVar() # "
iint = StringVar() # "
omlel = StringVar() # create argument of perilune string vars
omllb = StringVar() # "
omlub = StringVar() # "
omlint = StringVar() # "
omuel = StringVar() # create right angle of ascending node string vars
omulb = StringVar() # "
omuub = StringVar() # "
omuint = StringVar() # "
nuel = StringVar() # create mean anomaly string vars
nulb = StringVar() # "
nuub = StringVar() # "
nuint = StringVar() # "

ttk.Label(mainframe,text="rp").grid(column=2,row=2,sticky=(W,E)) # radius of perilune label

rp_entry = ttk.Entry(mainframe,width=7,textvariable=rpel,name="rp_entry") # radius of perilune entry
rp_entry.grid(column=3,row=2,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=2,sticky=(W,E)) # radius of perilune lb label
rplb_entry = ttk.Entry(mainframe,width=7,textvariable=rplb,name="rplb_entry") # radius of perilune lb entry
rplb_entry.grid(column=5,row=2,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=2,sticky=(W,E)) # radius of perilune ub label
rpub_entry = ttk.Entry(mainframe,width=7,textvariable=rpub,name="rpub_entry") # radius of perilune ub entry
rpub_entry.grid(column=7,row=2,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Interval").grid(column=8,row=2,sticky=(W,E)) # radius of perilune int label
rpint_entry = ttk.Entry(mainframe,width=7,textvariable=rpint,name="rpint_entry") # radius of perilune int entry
rpint_entry.grid(column=9,row=2,sticky=(W,E)) # place widget

###



ttk.Label(mainframe,text="e").grid(column=2,row=3,sticky=(W,E)) # eccentricity label

e_entry = ttk.Entry(mainframe,width=7,textvariable=eel,name="e_entry") # eccentricity entry
e_entry.grid(column=3,row=3,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=3,sticky=(W,E)) # eccentricity lb label
elb_entry = ttk.Entry(mainframe,width=7,textvariable=elb,name="elb_entry") # eccentricity lb entry
elb_entry.grid(column=5,row=3,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=3,sticky=(W,E)) # eccentricity ub label
eub_entry = ttk.Entry(mainframe,width=7,textvariable=eub,name="eub_entry") # eccentricity ub entry
eub_entry.grid(column=7,row=3,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Interval").grid(column=8,row=3,sticky=(W,E)) # eccentricity int label
eint_entry = ttk.Entry(mainframe,width=7,textvariable=eint,name="eint_entry") # eccentricity int entry
eint_entry.grid(column=9,row=3,sticky=(W,E)) # place widget

###



ttk.Label(mainframe,text="i").grid(column=2,row=4,sticky=(W,E)) # inclination label

i_entry = ttk.Entry(mainframe,width=7,textvariable=iel,name="i_entry") # inclination entry
i_entry.grid(column=3,row=4,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=4,sticky=(W,E)) # inclination lb label
ilb_entry = ttk.Entry(mainframe,width=7,textvariable=ilb,name="ilb_entry") # inclination lb entry
ilb_entry.grid(column=5,row=4,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=4,sticky=(W,E))# inclination ub label
iub_entry = ttk.Entry(mainframe,width=7,textvariable=iub,name="iub_entry") # inclination ub entry
iub_entry.grid(column=7,row=4,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Interval").grid(column=8,row=4,sticky=(W,E)) # inclination int label
iint_entry = ttk.Entry(mainframe,width=7,textvariable=iint,name="iint_entry") # inclination int entry
iint_entry.grid(column=9,row=4,sticky=(W,E)) # place widget

###



ttk.Label(mainframe,text= u'\u03C9').grid(column=2,row=5,sticky=(W,E)) # argument of perilune label

oml_entry = ttk.Entry(mainframe,width=7,textvariable=omlel,name="oml_entry") # argument of perilune entry
oml_entry.grid(column=3,row=5,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=5,sticky=(W,E)) # argument of lb perilune label
omllb_entry = ttk.Entry(mainframe,width=7,textvariable=omllb,name="omllb_entry") # argument of perilune lb entry
omllb_entry.grid(column=5,row=5,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=5,sticky=(W,E)) # argument of ub perilune label
omlub_entry = ttk.Entry(mainframe,width=7,textvariable=omlub,name="omlub_entry") # argument of perilune ub entry
omlub_entry.grid(column=7,row=5,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Interval").grid(column=8,row=5,sticky=(W,E)) # argument of perilune int label
omlint_entry = ttk.Entry(mainframe,width=7,textvariable=omlint,name="omlint_entry")# argument of perilune int entry
omlint_entry.grid(column=9,row=5,sticky=(W,E)) # place widget

###



ttk.Label(mainframe,text=u'\u03A9').grid(column=2,row=6,sticky=(W,E)) # right angle of ascending node label

omu_entry = ttk.Entry(mainframe,width=7,textvariable=omuel,name="omu_entry") # right angle of ascending node entry
omu_entry.grid(column=3,row=6,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=6,sticky=(W,E)) # right angle of ascending node lb label
omulb_entry = ttk.Entry(mainframe,width=7,textvariable=omulb,name="omulb_entry") # right angle of ascending node lb entry
omulb_entry.grid(column=5,row=6,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=6,sticky=(W,E)) # right angle of ascending node ub label
omuub_entry = ttk.Entry(mainframe,width=7,textvariable=omuub,name="omuub_entry") # right angle of ascending node ub entry
omuub_entry.grid(column=7,row=6,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Interval").grid(column=8,row=6,sticky=(W,E)) # right angle of ascending node int label
omuint_entry = ttk.Entry(mainframe,width=7,textvariable=omuint,name="omuint_entry") # right angle of ascending node int entry
omuint_entry.grid(column=9,row=6,sticky=(W,E)) # place widget

###



ttk.Label(mainframe,text=u'\u03BD').grid(column=2,row=7,sticky=(W,E)) # mean anomaly label

nu_entry = ttk.Entry(mainframe,width=7,textvariable=nuel,name="nu_entry") # mean anomaly entry
nu_entry.grid(column=3,row=7,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=7,sticky=(W,E)) # mean anomaly lb label
nulb_entry = ttk.Entry(mainframe,width=7,textvariable=nulb,name="nulb_entry") # mean anomaly lb entry
nulb_entry.grid(column=5,row=7,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=7,sticky=(W,E)) # mean anomaly ub label
nuub_entry = ttk.Entry(mainframe,width=7,textvariable=nuub,name="nuub_entry") # mean anomaly ub entry
nuub_entry.grid(column=7,row=7,sticky=(W,E)) # place widget

ttk.Label(mainframe,text="Interval").grid(column=8,row=7,sticky=(W,E)) # mean anomaly int label
nuint_entry = ttk.Entry(mainframe,width=7,textvariable=nuint,name="nuint_entry") # mean anomaly int entry
nuint_entry.grid(column=9,row=7,sticky=(W,E)) # place widget

###

masssat = StringVar() # satellite mass string variable
ttk.Label(mainframe,text="Satellite Mass (kg)").grid(column = 2,row = 8, sticky=(W,E)) # satellite mass label
mass_entry = ttk.Entry(mainframe,width=7,textvariable=masssat,name="masssat") # satellite mass entry
mass_entry.grid(column=3,row=8,sticky=(W,E)) # place widget

scenname = StringVar() # scenario name string variable
ttk.Label(mainframe,text="Scenario Name").grid(column=2,row=9,sticky=(W,E)) # scenario name label
scenname_entry = ttk.Entry(mainframe,width=14,textvariable=scenname,name="scenname") # scenario name entry
scenname_entry.grid(column=3,row=9,sticky=(W,E)) # place widget

ttk.Button(mainframe,text="Run",command=run).grid(column=2,row=10,sticky=W) # run button

for child in mainframe.winfo_children(): #sets padding properties for all the widgets in mainframe
    child.grid_configure(padx=5,pady=5)
rp_entry.focus() #puts cursor in entry when opened
root.bind("<Return>",run) #sets enter key to behave like run button

root.mainloop() #makes it do the things