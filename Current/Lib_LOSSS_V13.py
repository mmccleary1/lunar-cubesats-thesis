#GLOSSS (Graphical Lunar Orbital STK Simulation Software) 1.5
#Compatible with Lib_LOSSS_V12.py module
#6/8/23

## imports ##

import time
import os
import sys
from typing import Union, Tuple

try:
    from agi.stk12.stkdesktop import STKDesktop, STKDesktopApplication
    from agi.stk12.stkengine import STKEngine, STKEngineApplication
    from agi.stk12.stkobjects import *
    from agi.stk12.stkobjects.aviator import *
except:
    print("Failed to import stk modules. Make sure you have installed the STK Python API wheel \
        (agi.stk<..ver..>-py3-none-any.whl) from the STK Install bin directory")
try:
    import matplotlib.pyplot as plt
    import numpy as np
    import math
    from tkinter import * #import tkinter module- note that module not referenced later
    from tkinter import ttk #import ttk submodule for themed widgets
    import re
except:
    print("**** Error: Failed to import one of the required modules (matplotlib, numpy). \
        Make sure you have them installed. If you are using anaconda python, make sure you are running \
              from an anaconda command prompt.")
    sys.exit(1)

def initializeStk(scenarioName: str = "PythonApi", scenarioPath: str = "") -> Tuple[STKDesktopApplication, AgStkObjectRoot]:
    '''Return stk application and stk root
    A method to either start STK or STK Engine
    Optionally define a scenarioName or scenarioPath
    '''
    
    stk = STKDesktop.StartApplication(visible=True, userControl=True) # start up STK
    stkRoot = stk.Root # assign the root to a variable
    if not scenarioPath:
        if stkRoot.CurrentScenario is None: 
            stkRoot.NewScenario(scenarioName)
        else:
            stkRoot.CloseScenario()
            stkRoot.NewScenario(scenarioName)
    else:
        if stkRoot.CurrentScenario is not None:
            stkRoot.CloseScenario()
        try:
            stkRoot.Load(scenarioPath)
        except:
            print(f'Unable to load scenario: {scenarioPath}')
    setMetricUnits(stkRoot)
    return stk, stkRoot

def setMetricUnits(stkRoot: AgStkObjectRoot) -> None:
    '''
    Set relavent units to meters, seconds, and degrees
    '''
    stkRoot.UnitPreferences.SetCurrentUnit("Distance", "km")
    stkRoot.UnitPreferences.SetCurrentUnit("Time", "sec")
    stkRoot.UnitPreferences.SetCurrentUnit("Angle", "deg")
    stkRoot.UnitPreferences.SetCurrentUnit("Latitude", "deg")
    stkRoot.UnitPreferences.SetCurrentUnit("Longitude", "deg")
    stkRoot.UnitPreferences.SetCurrentUnit("Duration", "sec")
    
#---------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------Main--------------------------------------------------------#
#---------------------------------------------------------------------------------------------------------------#


def mainfun(scenarioName,newargs,mass,ind,stk,stkRoot,scenario) -> None: #function with main utility
    
    # unpack individual elements from tuple:
    rp = newargs[0] 
    ev = newargs[1]
    i = newargs[2]
    argp = newargs[3]
    raan = newargs[4]
    ma = newargs[5]
    
    # variable for setting the coordinate system to ICRF:
    coord = 11
    
    # name of text file for containing output:
    file = 'outorb' + scenarioName + '.txt'
    
    # open file for writing results:
    with open(file, 'w') as f:
        f.write('a,e,i,omega,Omega,nu,status \n')
    
        # satellite name:    
        str1 = "mysat" + str(ind)
    
        sat2 = stkRoot.CurrentScenario.Children.NewOnCentralBody(18, str1, "moon") #Create satellite called "mysat" in orbit around the moon- 18 is the object creation number of satellites
       
        # set the mass of the satellite based on user input
        massstr =  "SetMass */Satellite/* Value " + mass
        # execute the connect command:
        stkRoot.ExecuteCommand(massstr)
        # print the satellite name string as a status update:
        print(str1)
        
        # calculate radius of apilune:
        ra = (rp*(ev+1))/(1-ev)
        # calculate semi-major axis
        a = 0.5*(ra + rp)
        
        # set propagator type
        sat2.SetPropagatorType(0)  # ePropa gatorHPOP
        sat2.Propagator.Step = 60
        
        
        sat2.Propagator.InitialState.Representation.AssignClassical(coord, a, ev, i, argp, raan, ma)  # ICRF classical elements
        #Coordinate system, SemiMajorAxis, Eccentricity, Inclination, ArgOfPeriselenium, RAAN, MeanAnomaly
        # Set up force model:
        forceModel = sat2.Propagator.ForceModel # create variable to abbreviate path to force model
        forceModel.CentralBodyGravity.File = r'C:\Program Files\AGI\STK 12\STKData\CentralBodies\Moon\GL0660B.grv' #use Grail 0660B model for lunar gravity
        forceModel.CentralBodyGravity.MaxDegree = 48
        forceModel.CentralBodyGravity.MaxOrder = 48
        forceModel.Drag.Use = 0 # set drag to 0 b/c of negligible lunar atmosphere
        
        # set up integrator:
        integrator = sat2.Propagator.Integrator
        integrator.DoNotPropagateBelowAlt = 2
        integrator.IntegrationModel = 3
        integrator.StepSizeControl.Method = 1
        integrator.StepSizeControl.ErrorTolerance = 1e-13
        integrator.StepSizeControl.MinStepSize = 0.1
        integrator.StepSizeControl.MaxStepSize = 30
        integrator.Interpolation.Method = 1
        integrator.Interpolation.Order = 7
        #a,e,i,omega,Omega,nu,status
        # rp = newargs[0] 
        # ev = newargs[1]
        # i = newargs[2]
        # argp = newargs[3]
        # raan = newargs[4]
        # ma = newargs[5]
        ctxt = "{:.1f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, 0".format(ind,a,ev,i,argp,raan,ma)
        stxt = "{:.1f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, 1".format(ind,a,ev,i,argp,raan,ma)
        
                
        try:
            # attempt to propagate through the time frame:
            sat2.Propagator.Propagate()
            f.write(stxt + "\n")
            print(stxt)
        except:
            print("Oops! You crashed.")
            f.write(ctxt + "\n")
            print(ctxt)
        
        stkRoot.Rewind()
                        
        stkRoot.Save
