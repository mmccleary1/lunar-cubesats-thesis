#GLOSSS (Graphical Lunar Orbital STK Simulation Software) 1.0
#Compatible with Lib_LOSSS_V10.py module
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  8 19:08:53 2023

@author: mlmcc
"""

from Lib_LOSSS_V10 import *


# def calculate(*args):
#     try:
#         value = float(feet.get())
#         #meters.set(int(0.3048 * value * 10000.0 + 0.5)/10000.0)
#         meters.set(math.sqrt(value))
#     except ValueError:
#         pass
    

def run(*args):
    setMetricUnits()
    try:
        value = float(feet.get())
        #meters.set(int(0.3048 * value * 10000.0 + 0.5)/10000.0)
        meters.set(math.sqrt(value))
    except ValueError:
        pass    

# def calculate(*args):
#     file1 = open("test.txt","w")
#     L = ["This is Delhi \n","This is Paris \n","This is London \n"]
     
#     # \n is placed to indicate EOL (End of Line)
#     value = str(feet.get())
#     file1.write(value)
    
#     file1.close() #to change file access modes

def run(*args):
    mainfun()

root = Tk() #create main content window
root.title("GLOSSS V1.1") #set main content window title to "feet to meters" 

mainframe = ttk.Frame(root,padding="3 3 12 12") #create frame widget to hold contents
mainframe.grid(column=0,row=0,sticky=(N,W,E,S)) #place it in the main window
root.columnconfigure(0,weight=1) #expand frame to fill window if resized
root.rowconfigure(0,weight=1)

ttk.Label(mainframe,text="Initial Conditions (km and degrees)").grid(column=2,row=1,sticky=(W,E))

selected_ind = StringVar()
radioelem = ('a','e','i','oml','omu','nu')

i = 2
# radio buttons
for elem in radioelem:
    r = ttk.Radiobutton(
        mainframe,
        value=elem,
        variable=selected_ind
    )
    r.grid(column=1,row=i,sticky=(W,E))
    #r.pack(fill='x', padx=5, pady=5)
    i = i + 1

ael = StringVar()
ttk.Label(mainframe,text="a").grid(column = 2,row=2,sticky=(W,E)) #this should really be a for loop
a_entry = ttk.Entry(mainframe,width=7,textvariable=ael)
a_entry.grid(column=3,row=2,sticky=(W,E))

elb = StringVar()
eub = StringVar()
eint = StringVar()

ttk.Label(mainframe,text="e").grid(column=2,row=3,sticky=(W,E))
ttk.Label(mainframe,text="lower bound").grid(column=3,row=3,sticky=(W,E))
elb_entry = ttk.Entry(mainframe,width=7,textvariable=elb)
elb_entry.grid(column=4,row=3,sticky=(W,E))
ttk.Label(mainframe,text="Upper bound").grid(column=5,row=3,sticky=(W,E))
eub_entry = ttk.Entry(mainframe,width=7,textvariable=eub)
eub_entry.grid(column=6,row=3,sticky=(W,E))
ttk.Label(mainframe,text="Interval").grid(column=7,row=3,sticky=(W,E))
eint_entry = ttk.Entry(mainframe,width=7,textvariable=eint)
eint_entry.grid(column=8,row=3,sticky=(W,E))

iel = StringVar()
ttk.Label(mainframe,text="i").grid(column=2,row=4,sticky=(W,E))
i_entry = ttk.Entry(mainframe,width=7,textvariable=iel)
i_entry.grid(column=3,row=4,sticky=(W,E))

omlel = StringVar()
ttk.Label(mainframe,text="om").grid(column=2,row=5,sticky=(W,E))
oml_entry = ttk.Entry(mainframe,width=7,textvariable=omlel)
oml_entry.grid(column=3,row=5,sticky=(W,E))

omuel = StringVar()
ttk.Label(mainframe,text="OM").grid(column=2,row=6,sticky=(W,E))
omu_entry = ttk.Entry(mainframe,width=7,textvariable=omuel)
omu_entry.grid(column=3,row=6,sticky=(W,E))

nuel = StringVar()
ttk.Label(mainframe,text="nu").grid(column=2,row=7,sticky=(W,E))
nu_entry = ttk.Entry(mainframe,width=7,textvariable=nuel)
nu_entry.grid(column=3,row=7,sticky=(W,E))

ttk.Button(mainframe,text="Run",command=run).grid(column=2,row=8,sticky=W)

for child in mainframe.winfo_children(): #sets padding properties for all the widgets in mainframe
    child.grid_configure(padx=5,pady=5)
a_entry.focus() #puts cursor in entry when opened
root.bind("<Return>",run) #sets enter key to behave like run button

root.mainloop() #makes it do the things