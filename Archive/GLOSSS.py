#GLOSSS (Graphical Lunar Orbital STK Simulation Software) 1.0
#Compatible with CLOSS_V30.py module
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  8 19:08:53 2023

@author: mlmcc
"""

from tkinter import * #import tkinter module- note that module not referenced later
from tkinter import ttk #import ttk submodule for themed widgets
import math

from LCOSim_V20 import *


# def calculate(*args):
#     try:
#         value = float(feet.get())
#         #meters.set(int(0.3048 * value * 10000.0 + 0.5)/10000.0)
#         meters.set(math.sqrt(value))
#     except ValueError:
#         pass
    
def run(*args):
    try:
        value = float(feet.get())
        #meters.set(int(0.3048 * value * 10000.0 + 0.5)/10000.0)
        meters.set(math.sqrt(value))
    except ValueError:
        pass    

# def calculate(*args):
#     file1 = open("test.txt","w")
#     L = ["This is Delhi \n","This is Paris \n","This is London \n"]
     
#     # \n is placed to indicate EOL (End of Line)
#     value = str(feet.get())
#     file1.write(value)
    
#     file1.close() #to change file access modes

root = Tk() #create main content window
root.title("Feet to Meters") #set main content window title to "feet to meters" 

mainframe = ttk.Frame(root,padding="3 3 12 12") #create frame widget to hold contents
mainframe.grid(column=0,row=0,sticky=(N,W,E,S)) #place it in the main window
root.columnconfigure(0,weight=1) #expand frame to fill window if resized
root.rowconfigure(0,weight=1)

feet = StringVar()
feet_entry = ttk.Entry(mainframe,width=7,textvariable=feet) #create entry widget, specify parent, width, and textvariable
feet_entry.grid(column=2,row=1,sticky=(W,E)) #place widget on screen, position it, and line it up w/in cell

meters = StringVar()
ttk.Label(mainframe,text="Initial Conditions").grid(column=1,row=1,sticky=(W,E))

ttk.Label(mainframe,text="a").grid(column = 1,row=2,sticky=(W,E))
a_entry = ttk.Entry(mainframe,width=7,textvariable=ael)
a_entry.grid(column=2,row=2,sticky=(W,E))

ttk.Label(mainframe,text="e").grid(column=1,row=3,sticky=(W,E))
e_entry = ttk.Entry(mainframe,width=7,textvariable=eel)
e_entry.grid(column=2,row=3,sticky=(W,E))

ttk.Label(mainframe,text="i").grid(column=1,row=4,sticky=(W,e))
i_entry = ttk.Entry(mainframe,width=7,textvariable=iel)
i_entry.grid(column=2,row=4,sticky=(W,E))

ttk.Label(mainframe,text="om").grid(column=1,row=5,sticky=(W,e))
oml_entry = ttk.Entry(mainframe,width=7,textvariable=ael)
oml_entry.grid(column=2,row=5,sticky=(W,E))

ttk.Label(mainframe,text="OM").grid(column=1,row=6,sticky=(W,e))
omu_entry = ttk.Entry(mainframe,width=7,textvariable=ael)
omu_entry.grid(column=2,row=6,sticky=(W,E))

ttk.Label(mainframe,text="nu").grid(column=1,row=7,sticky=(W,e))
nu_entry = ttk.Entry(mainframe,width=7,textvariable=ael)
nu_entry.grid(column=2,row=7,sticky=(W,E))

ttk.Button(mainframe,text="Run",command=run).grid(column=1,row=8,sticky=W)

ttk.Label(mainframe,text="feet").grid(column=3,row=1,sticky=W)
ttk.Label(mainframe,text="is equivalent to").grid(column=1,row=2,sticky=E)
ttk.Label(mainframe,text="meters").grid(column=3,row=2,sticky=W)

for child in mainframe.winfo_children(): #sets padding properties for all the widgets in mainframe
    child.grid_configure(padx=5,pady=5)
feet_entry.focus() #puts cursor in entry when opened
root.bind("<Return>",calculate) #sets enter key to behave like calculate button

root.mainloop() #makes it do the things