
import time
import os
import sys
from typing import Union, Tuple

try:
    from agi.stk12.stkdesktop import STKDesktop, STKDesktopApplication
    from agi.stk12.stkengine import STKEngine, STKEngineApplication
    from agi.stk12.stkobjects import *
    from agi.stk12.stkobjects.aviator import *
except:
    print("Failed to import stk modules. Make sure you have installed the STK Python API wheel \
        (agi.stk<..ver..>-py3-none-any.whl) from the STK Install bin directory")
try:
    import matplotlib.pyplot as plt
    import numpy as np
    import math
    from tkinter import * #import tkinter module- note that module not referenced later
    from tkinter import ttk #import ttk submodule for themed widgets
except:
    print("**** Error: Failed to import one of the required modules (matplotlib, numpy). \
        Make sure you have them installed. If you are using anaconda python, make sure you are running \
              from an anaconda command prompt.")
    sys.exit(1)

#---------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------Variables-----------------------------------------------------#
#---------------------------------------------------------------------------------------------------------------#

# Scenario Name
scenarioName = "LOS20"
coord = 11

e = [0 for x in range(2)]
for k in range(2): e[k] = k*0.1

rp = 1836.1
i = 90
argp = 90
raan = 0
ma = 0
j = 0

#---------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------Defs-------------------------------------------------------#
#---------------------------------------------------------------------------------------------------------------#

def initializeStk(scenarioName: str = "PythonApi", scenarioPath: str = "") -> Tuple[STKDesktopApplication, AgStkObjectRoot]:
    '''Return stk application and stk root
    A method to either start STK or STK Engine
    Optionally define a scenarioName or scenarioPath
    '''
    
    stk = STKDesktop.StartApplication(visible=True, userControl=True)
    stkRoot = stk.Root
    if not scenarioPath:
        if stkRoot.CurrentScenario is None:
            stkRoot.NewScenario(scenarioName)
        else:
            stkRoot.CloseScenario()
            stkRoot.NewScenario(scenarioName)
    else:
        if stkRoot.CurrentScenario is not None:
            stkRoot.CloseScenario()
        try:
            stkRoot.Load(scenarioPath)
        except:
            print(f'Unable to load scenario: {scenarioPath}')
    setMetricUnits(stkRoot)
    return stk, stkRoot

def setMetricUnits(stkRoot: AgStkObjectRoot) -> None:
    '''
    Set relavent units to meters, seconds, and degrees
    '''
    stkRoot.UnitPreferences.SetCurrentUnit("Distance", "km")
    stkRoot.UnitPreferences.SetCurrentUnit("Time", "sec")
    stkRoot.UnitPreferences.SetCurrentUnit("Angle", "deg")
    stkRoot.UnitPreferences.SetCurrentUnit("Latitude", "deg")
    stkRoot.UnitPreferences.SetCurrentUnit("Longitude", "deg")
    #stkRoot.UnitPreferences.SetCurrentUnit("DateFormat", "EpSec")
    stkRoot.UnitPreferences.SetCurrentUnit("Duration", "sec")
    
#---------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------Main--------------------------------------------------------#
#---------------------------------------------------------------------------------------------------------------#

# Grab Root and set units

def mainfun(*args) -> None:
    stk, stkRoot = initializeStk(scenarioName) #Opens STK and creates scenario named according to scenarioName variable
    scenario = stkRoot.CurrentScenario #Assigns new scenario to scenario variable
    
    scenario.SetTimePeriod('29 Aug 2022 17:00:00.000', '+2 years') # times are UTCG
    stkRoot.Rewind()
    
    #satellite = stkRoot.CurrentScenario.Children.New(18, 'MySatellite')  # eSatellite
    
    facility = stkRoot.CurrentScenario.Children.NewOnCentralBody(8, "myfac", "moon") #Create facility object (8) on lunar surface
    
    # IAgFacility facility: Facility Object
    facility.Position.AssignGeodetic(-90, 45, 0)  #Assign LLA position (selenographic coordinates) of facility to the south pole at ground level
    
    # Set altitude to height of terrain
    facility.UseTerrain = True #Use lunar terrain to determine height of facility position
    
    # Set altitude to a distance above the ground
    facility.HeightAboveGround = 0.0  # km, Assign facility height to 0
    
    j = range(len(e))
    ind = 0
    
    with open('outorb.txt', 'w') as f:
        f.write('index,e,status \n')
    
        for ev in e:
            
            str1 = "mysat" + str(ind)
            #str2 = "mysen" + str(ind)
        
            sat2 = stkRoot.CurrentScenario.Children.NewOnCentralBody(18, str1, "moon") #Create satellite called "mysat" in orbit around the moon- 18 is the object creation number of satellites
            
            print(str1)
            
    # =============================================================================
    #         sensor = sat2.Children.New(20, str2)  #Create sensor object (20) as a child of the satellite object
    #         sensor.CommonTasks.SetPatternEOIR(0.1,AgESnEOIRProcessingLevels.eSensorOutput)
    #         
    #         sensor.SetPointingType(5) #Set sensor pointing type to targeted (5)
    #         
    #         sensor.CommonTasks.SetPointingTargetedTracking(AgETrackModeType.eTrackModeTranspond, AgEBoresightType.eBoresightRotate, facility.Path)
    # =============================================================================
            
            ra = (rp*(ev+1))/(1-ev)
            a = ra + rp
            
            sat2.SetPropagatorType(0)  # ePropagatorHPOP
            sat2.Propagator.Step = 60
            
            #sat2.Propagator.InitialState.Representation.AssignClassical(11, 1836.1, 0, 0, 0, 0, 0)  # ICRF classical elements
            sat2.Propagator.InitialState.Representation.AssignClassical(coord, 0.5*a, ev, i, argp, raan, ma)  # ICRF classical elements
            #Coordinate system, SemiMajorAxis, Eccentricity, Inclination, ArgOfPeriselenium, RAAN, MeanAnomaly
            forceModel = sat2.Propagator.ForceModel
            #forceModel.CentralBodyGravity.File = r'C:\Program Files\AGI\STK 12\STKData\CentralBodies\Earth\WGS84_EGM96.grv'
            forceModel.CentralBodyGravity.File = r'C:\Program Files\AGI\STK 12\STKData\CentralBodies\Moon\GL0660B.grv'
            forceModel.CentralBodyGravity.MaxDegree = 48
            forceModel.CentralBodyGravity.MaxOrder = 48
            forceModel.Drag.Use = 0
            # =============================================================================
            # forceModel.Drag.DragModel.Cd = 0.01
            # forceModel.Drag.DragModel.AreaMassRatio = 0.01
            # forceModel.SolarRadiationPressure.Use = 0
            # =============================================================================
            
            integrator = sat2.Propagator.Integrator
            integrator.DoNotPropagateBelowAlt = -1e6
            integrator.IntegrationModel = 3
            integrator.StepSizeControl.Method = 1
            integrator.StepSizeControl.ErrorTolerance = 1e-13
            integrator.StepSizeControl.MinStepSize = 0.1
            integrator.StepSizeControl.MaxStepSize = 30
            integrator.Interpolation.Method = 1
            integrator.Interpolation.Order = 7
            
            ctxt = "{:.1f}, {:.2f}, 0".format(ind,ev)
            stxt = "{:.1f}, {:.2f}, 1".format(ind,ev)
            
            try:
                sat2.Propagator.Propagate()
            except:
                print("Oops! You crashed.")
                f.write(ctxt + "\n")
                print(ctxt)
            
            f.write(stxt + "\n")
            print(stxt)
            
            stkRoot.Rewind()
                
            ind = ind + 1
        
    stkRoot.Save
    
    runtime = (time.process_time())/60
    print(str(runtime) + " min")
    
#mainfun()