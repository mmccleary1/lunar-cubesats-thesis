import time
import os
import sys
from typing import Union, Tuple

try:
    from agi.stk12.stkdesktop import STKDesktop, STKDesktopApplication
    from agi.stk12.stkengine import STKEngine, STKEngineApplication
    from agi.stk12.stkobjects import *
    from agi.stk12.stkobjects.aviator import *
except:
    print("Failed to import stk modules. Make sure you have installed the STK Python API wheel \
        (agi.stk<..ver..>-py3-none-any.whl) from the STK Install bin directory")
try:
    import matplotlib.pyplot as plt
    import numpy as np
    import math
except:
    print("**** Error: Failed to import one of the required modules (matplotlib, numpy). \
        Make sure you have them installed. If you are using anaconda python, make sure you are running \
             from an anaconda command prompt.")
    sys.exit(1)

#---------------------------------------------------------------------------------------------------------------#
#-------------------------------------------------Variables-----------------------------------------------------#
#---------------------------------------------------------------------------------------------------------------#

# Scenario Name
scenarioName = "test"

#---------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------Defs-------------------------------------------------------#
#---------------------------------------------------------------------------------------------------------------#

def initializeStk(scenarioName: str = "PythonApi", scenarioPath: str = "") -> Tuple[STKDesktopApplication, AgStkObjectRoot]:
    '''Return stk application and stk root
    A method to either start STK or STK Engine
    Optionally define a scenarioName or scenarioPath
    '''
    
    stk = STKDesktop.StartApplication(visible=True, userControl=True)
    stkRoot = stk.Root
    if not scenarioPath:
        if stkRoot.CurrentScenario is None:
            stkRoot.NewScenario(scenarioName)
        else:
            stkRoot.CloseScenario()
            stkRoot.NewScenario(scenarioName)
    else:
        if stkRoot.CurrentScenario is not None:
            stkRoot.CloseScenario()
        try:
            stkRoot.Load(scenarioPath)
        except:
            print(f'Unable to load scenario: {scenarioPath}')
    setMetricUnits(stkRoot)
    return stk, stkRoot

def setMetricUnits(stkRoot: AgStkObjectRoot) -> None:
    '''
    Set relavent units to meters, seconds, and degrees
    '''
    stkRoot.UnitPreferences.SetCurrentUnit("Distance", "km")
    stkRoot.UnitPreferences.SetCurrentUnit("Time", "sec")
    stkRoot.UnitPreferences.SetCurrentUnit("Angle", "deg")
    stkRoot.UnitPreferences.SetCurrentUnit("Latitude", "deg")
    stkRoot.UnitPreferences.SetCurrentUnit("Longitude", "deg")
    #stkRoot.UnitPreferences.SetCurrentUnit("DateFormat", "EpSec")
    stkRoot.UnitPreferences.SetCurrentUnit("Duration", "sec")

#---------------------------------------------------------------------------------------------------------------#
#---------------------------------------------------Main--------------------------------------------------------#
#---------------------------------------------------------------------------------------------------------------#

# Grab Root and set units
stk, stkRoot = initializeStk(scenarioName) #Opens STK and creates scenario named according to scenarioName variable
scenario = stkRoot.CurrentScenario #Assigns new scenario to scenario variable

scenario.SetTimePeriod('29 Aug 2022 17:00:00.000', '+2 years') # times are UTCG
stkRoot.Rewind()

#satellite = stkRoot.CurrentScenario.Children.New(18, 'MySatellite')  # eSatellite
j = range(6)
for i in j:
    str1 = "mysat" + str(i)
    str2 = "mysensor" + str(i)
    
    sat2 = stkRoot.CurrentScenario.Children.NewOnCentralBody(18, str1, "moon") #Create satellite called "mysat" in orbit around the moon- 18 is the object creation number of satellites
    
    sensor = sat2.Children.New(20, str2)  #Create sensor object (20) as a child of the satellite object
    sensor.CommonTasks.SetPatternEOIR(0.1,AgESnEOIRProcessingLevels.eSensorOutput)
    
    sensor.SetPointingType(5) #Set sensor pointing type to targeted (5)
    
 #   sensor.CommonTasks.SetPointingTargetedTracking(AgETrackModeType.eTrackModeTranspond, AgEBoresightType.eBoresightRotate, facility.Path)