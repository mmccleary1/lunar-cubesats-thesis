#GLOSSS (Graphical Lunar Orbital STK Simulation Software) 1.0
#Compatible with Lib_LOSSS_V10.py module
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  8 19:08:53 2023

@author: mlmcc
"""

from Lib_LOSSS_V10 import *

def select(*args):
    print(int(selected_ind.get()))
    # rp_entry.config(state= "disabled")
    # rp_entry.config(state= "enabled")

    for child in mainframe.winfo_children():
        if isinstance(child, ttk.Entry) and str(child) != ".!frame.masssat" and str(child) != ".!frame.scenname":
            child.config(state= "disabled")
            print(str(child))

def run(*args):
    erange = float(eub.get()) - float(elb.get())
    elen = (erange / float(eint.get())) + 1
    ev = [0 for x in range(int(elen))]
    for k in range(int(elen)): ev[k] = float(elb.get()) + k*float(eint.get())
    mainfun(float(rpel.get()),ev,float(iel.get()),float(omlel.get()),float(omuel.get()),float(nuel.get()),str(scenname.get()),str(masssat.get()))
    
root = Tk() #create main content window
root.title("GLOSSS V1.2") #set main content window title to "GLOSSS VX.X" 

mainframe = ttk.Frame(root,padding="3 3 12 12") #create frame widget to hold contents
mainframe.grid(column=0,row=0,sticky=(N,W,E,S)) #place it in the main window
root.columnconfigure(0,weight=1) #expand frame to fill window if resized
root.rowconfigure(0,weight=1)

ttk.Label(mainframe,text="Initial Conditions (km and degrees)").grid(column=2,row=1,sticky=(W,E))

selected_ind = StringVar()
radioelem = ('rp','e','i','oml','omu','nu')
m = 1
j = 2
# radio buttons
for elem in radioelem:
    r = ttk.Radiobutton(
        mainframe,
        value=m,
        variable=selected_ind,
        command = select
    )
    r.grid(column=1,row=j,sticky=(W,E))
    j = j + 1
    m = m + 1

###

rpel = StringVar()
rplb = StringVar()
rpub = StringVar()
rpint = StringVar()

ttk.Label(mainframe,text="rp").grid(column=2,row=2,sticky=(W,E))

rp_entry = ttk.Entry(mainframe,width=7,textvariable=rpel,name="rp_entry")
rp_entry.grid(column=3,row=2,sticky=(W,E))

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=2,sticky=(W,E))
rplb_entry = ttk.Entry(mainframe,width=7,textvariable=rplb,name="rplb_entry")
rplb_entry.grid(column=5,row=2,sticky=(W,E))

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=2,sticky=(W,E))
rpub_entry = ttk.Entry(mainframe,width=7,textvariable=rpub,name="rpub_entry")
rpub_entry.grid(column=7,row=2,sticky=(W,E))

ttk.Label(mainframe,text="Interval").grid(column=8,row=2,sticky=(W,E))
rpint_entry = ttk.Entry(mainframe,width=7,textvariable=rpint,name="rpint_entry")
rpint_entry.grid(column=9,row=2,sticky=(W,E))

###

eel = StringVar()
elb = StringVar()
eub = StringVar()
eint = StringVar()

ttk.Label(mainframe,text="e").grid(column=2,row=3,sticky=(W,E))

e_entry = ttk.Entry(mainframe,width=7,textvariable=eel,name="e_entry")
e_entry.grid(column=3,row=3,sticky=(W,E))

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=3,sticky=(W,E))
elb_entry = ttk.Entry(mainframe,width=7,textvariable=elb,name="elb_entry")
elb_entry.grid(column=5,row=3,sticky=(W,E))

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=3,sticky=(W,E))
eub_entry = ttk.Entry(mainframe,width=7,textvariable=eub,name="eub_entry")
eub_entry.grid(column=7,row=3,sticky=(W,E))

ttk.Label(mainframe,text="Interval").grid(column=8,row=3,sticky=(W,E))
eint_entry = ttk.Entry(mainframe,width=7,textvariable=eint,name="eint_entry")
eint_entry.grid(column=9,row=3,sticky=(W,E))

###

iel = StringVar()
ilb = StringVar()
iub = StringVar()
iint = StringVar()

ttk.Label(mainframe,text="i").grid(column=2,row=4,sticky=(W,E))

i_entry = ttk.Entry(mainframe,width=7,textvariable=iel,name="i_entry")
i_entry.grid(column=3,row=4,sticky=(W,E))

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=4,sticky=(W,E))
ilb_entry = ttk.Entry(mainframe,width=7,textvariable=ilb,name="ilb_entry")
ilb_entry.grid(column=5,row=4,sticky=(W,E))

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=4,sticky=(W,E))
iub_entry = ttk.Entry(mainframe,width=7,textvariable=iub,name="iub_entry")
iub_entry.grid(column=7,row=4,sticky=(W,E))

ttk.Label(mainframe,text="Interval").grid(column=8,row=4,sticky=(W,E))
iint_entry = ttk.Entry(mainframe,width=7,textvariable=iint,name="iint_entry")
iint_entry.grid(column=9,row=4,sticky=(W,E))

###

omlel = StringVar()
omllb = StringVar()
omlub = StringVar()
omlint = StringVar()

ttk.Label(mainframe,text="om").grid(column=2,row=5,sticky=(W,E))

oml_entry = ttk.Entry(mainframe,width=7,textvariable=omlel,name="oml_entry")
oml_entry.grid(column=3,row=5,sticky=(W,E))

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=5,sticky=(W,E))
omllb_entry = ttk.Entry(mainframe,width=7,textvariable=omllb,name="omllb_entry")
omllb_entry.grid(column=5,row=5,sticky=(W,E))

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=5,sticky=(W,E))
omlub_entry = ttk.Entry(mainframe,width=7,textvariable=omlub,name="omllb_entry")
omlub_entry.grid(column=7,row=5,sticky=(W,E))

ttk.Label(mainframe,text="Interval").grid(column=8,row=5,sticky=(W,E))
omlint_entry = ttk.Entry(mainframe,width=7,textvariable=omlint,name="omllb_entry")
omlint_entry.grid(column=9,row=5,sticky=(W,E))

###

omuel = StringVar()
omulb = StringVar()
omuub = StringVar()
omuint = StringVar()

ttk.Label(mainframe,text="OM").grid(column=2,row=6,sticky=(W,E))

omu_entry = ttk.Entry(mainframe,width=7,textvariable=omuel,name="omu_entry")
omu_entry.grid(column=3,row=6,sticky=(W,E))

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=6,sticky=(W,E))
omulb_entry = ttk.Entry(mainframe,width=7,textvariable=omulb,name="omulb_entry")
omulb_entry.grid(column=5,row=6,sticky=(W,E))

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=6,sticky=(W,E))
omuub_entry = ttk.Entry(mainframe,width=7,textvariable=omuub,name="omuub_entry")
omuub_entry.grid(column=7,row=6,sticky=(W,E))

ttk.Label(mainframe,text="Interval").grid(column=8,row=6,sticky=(W,E))
omuint_entry = ttk.Entry(mainframe,width=7,textvariable=omuint,name="omuint_entry")
omuint_entry.grid(column=9,row=6,sticky=(W,E))

###

nuel = StringVar()
nulb = StringVar()
nuub = StringVar()
nuint = StringVar()

ttk.Label(mainframe,text="nu").grid(column=2,row=7,sticky=(W,E))

nu_entry = ttk.Entry(mainframe,width=7,textvariable=iel,name="nu_entry")
nu_entry.grid(column=3,row=7,sticky=(W,E))

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=7,sticky=(W,E))
nulb_entry = ttk.Entry(mainframe,width=7,textvariable=nulb,name="nulb_entry")
nulb_entry.grid(column=5,row=7,sticky=(W,E))

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=7,sticky=(W,E))
nuub_entry = ttk.Entry(mainframe,width=7,textvariable=nuub,name="nuub_entry")
nuub_entry.grid(column=7,row=7,sticky=(W,E))

ttk.Label(mainframe,text="Interval").grid(column=8,row=7,sticky=(W,E))
nuint_entry = ttk.Entry(mainframe,width=7,textvariable=nuint,name="nuint_entry")
nuint_entry.grid(column=9,row=7,sticky=(W,E))

###

masssat = StringVar()
ttk.Label(mainframe,text="Satellite Mass (kg)").grid(column = 2,row = 8, sticky=(W,E))
mass_entry = ttk.Entry(mainframe,width=7,textvariable=masssat,name="masssat")
mass_entry.grid(column=3,row=8,sticky=(W,E))

scenname = StringVar()
ttk.Label(mainframe,text="Scenario Name").grid(column=2,row=9,sticky=(W,E))
scenname_entry = ttk.Entry(mainframe,width=14,textvariable=scenname,name="scenname")
scenname_entry.grid(column=3,row=9,sticky=(W,E))

ttk.Button(mainframe,text="Run",command=run).grid(column=2,row=10,sticky=W)

for child in mainframe.winfo_children(): #sets padding properties for all the widgets in mainframe
    child.grid_configure(padx=5,pady=5)
rp_entry.focus() #puts cursor in entry when opened
root.bind("<Return>",run) #sets enter key to behave like run button

root.mainloop() #makes it do the things