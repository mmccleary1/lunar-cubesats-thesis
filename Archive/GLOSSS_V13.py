#GLOSSS (Graphical Lunar Orbital STK Simulation Software) 1.3
#Compatible with Lib_LOSSS_V10.py module
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  8 19:08:53 2023

@author: mlmcc
"""

from Lib_LOSSS_V11 import *

def select(*args):
    
    bstr = "lb|ub|int"

    indstr = "\." + radioelem[int(selected_ind.get())]
    #print(indstr)

    for child in mainframe.winfo_children():
        
        mbd = re.search(bstr,str(child))
        mind = re.search(indstr,str(child))
        if isinstance(child, ttk.Entry) and mbd is not None and mind is None: #str(child) != ".!frame.masssat" and str(child) != ".!frame.scenname":
            child.config(state= "disabled")
            #print("1 " + str(child))
        elif isinstance(child, ttk.Entry) and mbd is None and mind is not None:
            child.config(state= "disabled")
            #print("2 " + str(child))
        else:
            child.config(state= "enabled")
            #print("3 " + str(child))
        
# =============================================================================
#         for elem in radioelem:
#             teststr = "\." + elem
#             
#             
# =============================================================================
        
def assign(*args):
    print("stuff")
    rpassign = float(rpel.get()) if rpel.get() != '' else 0
    eassign = float(eel.get()) if eel.get() != '' else 0
    iassign = float(iel.get()) if iel.get() != '' else 0
    omlassign = float(omlel.get()) if omlel.get() != '' else 0
    omuassign = float(omuel.get()) if omuel.get() != '' else 0
    nuassign = float(nuel.get()) if nuel.get() != '' else 0
    #newargs = [0,0,0,0,0,0]
    return [rpassign,eassign,iassign,omlassign,omuassign,nuassign]

def run(*args):
    
    #print("check1")
    
    
# =============================================================================
#     for child in mainframe.winfo_children():
#         cind = 0
#         for elem in radioelem:
#         
#             teststr = "\." + elem
#             mts = re.search(teststr,str(child))
#             
#             if isinstance(child, ttk.Entry) and mts is not None and cind != int(selected_ind.get())
#     
#                 myVars = globals()
#                 myVars[elem] = float()
#                 
#             cind = cind + 1
# =============================================================================
    
    newargs = assign()
    for thing in newargs: print(str(thing))
    stk, stkRoot = initializeStk(str(scenname.get())) #Opens STK and creates scenario named according to scenarioName variable
    scenario = stkRoot.CurrentScenario #Assigns new scenario to scenario variable
    
    scenario.SetTimePeriod('29 Aug 2022 17:00:00.000', '+2 years') # times are UTCG
    stkRoot.Rewind()
    
    #satellite = stkRoot.CurrentScenario.Children.New(18, 'MySatellite')  # eSatellite
    
    facility = stkRoot.CurrentScenario.Children.NewOnCentralBody(8, "myfac", "moon") #Create facility object (8) on lunar surface
    
    # IAgFacility facility: Facility Object
    facility.Position.AssignGeodetic(-90, 45, 0)  #Assign LLA position (selenographic coordinates) of facility to the south pole at ground level
    
    # Set altitude to height of terrain
    facility.UseTerrain = True #Use lunar terrain to determine height of facility position
    
    # Set altitude to a distance above the ground
    facility.HeightAboveGround = 0.0  # km, Assign facility height to 0
    
    status = 0
    ind = 0
    erange = float(eub.get()) - float(elb.get())
    elen = (erange / float(eint.get())) + 1
    ev = [0 for x in range(int(elen))]
    for k in range(int(elen)): ev[k] = float(elb.get()) + k*float(eint.get())
    for e in ev:
        mainfun(str(scenname.get()),float(rpel.get()),e,float(iel.get()),float(omlel.get()),float(omuel.get()),float(nuel.get()),str(masssat.get()),ind,stk,stkRoot,scenario)
        status = 1
        ind = ind+1
        #print("check3")
root = Tk() #create main content window
root.title("GLOSSS V1.3") #set main content window title to "GLOSSS VX.X" 

mainframe = ttk.Frame(root,padding="3 3 12 12") #create frame widget to hold contents
mainframe.grid(column=0,row=0,sticky=(N,W,E,S)) #place it in the main window
root.columnconfigure(0,weight=1) #expand frame to fill window if resized
root.rowconfigure(0,weight=1)

ttk.Label(mainframe,text="Initial Conditions (km and degrees)").grid(column=2,row=1,sticky=(W,E))

selected_ind = StringVar()
radioelem = ('rp','e','i','oml','omu','nu')
m = 0
j = 2
# radio buttons
for elem in radioelem:
    r = ttk.Radiobutton(
        mainframe,
        value=m,
        variable=selected_ind,
        command = select
    )
    r.grid(column=1,row=j,sticky=(W,E))
    j = j + 1
    m = m + 1
    
###

rpel = StringVar()
rplb = StringVar()
rpub = StringVar()
rpint = StringVar()

ttk.Label(mainframe,text="rp").grid(column=2,row=2,sticky=(W,E))

rp_entry = ttk.Entry(mainframe,width=7,textvariable=rpel,name="rp_entry")
rp_entry.grid(column=3,row=2,sticky=(W,E))

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=2,sticky=(W,E))
rplb_entry = ttk.Entry(mainframe,width=7,textvariable=rplb,name="rplb_entry")
rplb_entry.grid(column=5,row=2,sticky=(W,E))

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=2,sticky=(W,E))
rpub_entry = ttk.Entry(mainframe,width=7,textvariable=rpub,name="rpub_entry")
rpub_entry.grid(column=7,row=2,sticky=(W,E))

ttk.Label(mainframe,text="Interval").grid(column=8,row=2,sticky=(W,E))
rpint_entry = ttk.Entry(mainframe,width=7,textvariable=rpint,name="rpint_entry")
rpint_entry.grid(column=9,row=2,sticky=(W,E))

###

eel = StringVar()
elb = StringVar()
eub = StringVar()
eint = StringVar()

ttk.Label(mainframe,text="e").grid(column=2,row=3,sticky=(W,E))

e_entry = ttk.Entry(mainframe,width=7,textvariable=eel,name="e_entry")
e_entry.grid(column=3,row=3,sticky=(W,E))

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=3,sticky=(W,E))
elb_entry = ttk.Entry(mainframe,width=7,textvariable=elb,name="elb_entry")
elb_entry.grid(column=5,row=3,sticky=(W,E))

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=3,sticky=(W,E))
eub_entry = ttk.Entry(mainframe,width=7,textvariable=eub,name="eub_entry")
eub_entry.grid(column=7,row=3,sticky=(W,E))

ttk.Label(mainframe,text="Interval").grid(column=8,row=3,sticky=(W,E))
eint_entry = ttk.Entry(mainframe,width=7,textvariable=eint,name="eint_entry")
eint_entry.grid(column=9,row=3,sticky=(W,E))

###

iel = StringVar()
ilb = StringVar()
iub = StringVar()
iint = StringVar()

ttk.Label(mainframe,text="i").grid(column=2,row=4,sticky=(W,E))

i_entry = ttk.Entry(mainframe,width=7,textvariable=iel,name="i_entry")
i_entry.grid(column=3,row=4,sticky=(W,E))

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=4,sticky=(W,E))
ilb_entry = ttk.Entry(mainframe,width=7,textvariable=ilb,name="ilb_entry")
ilb_entry.grid(column=5,row=4,sticky=(W,E))

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=4,sticky=(W,E))
iub_entry = ttk.Entry(mainframe,width=7,textvariable=iub,name="iub_entry")
iub_entry.grid(column=7,row=4,sticky=(W,E))

ttk.Label(mainframe,text="Interval").grid(column=8,row=4,sticky=(W,E))
iint_entry = ttk.Entry(mainframe,width=7,textvariable=iint,name="iint_entry")
iint_entry.grid(column=9,row=4,sticky=(W,E))

###

omlel = StringVar()
omllb = StringVar()
omlub = StringVar()
omlint = StringVar()

#ttk.Label(mainframe,text="om").grid(column=2,row=5,sticky=(W,E))
ttk.Label(mainframe,text= u'\u03C9').grid(column=2,row=5,sticky=(W,E))

oml_entry = ttk.Entry(mainframe,width=7,textvariable=omlel,name="oml_entry")
oml_entry.grid(column=3,row=5,sticky=(W,E))

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=5,sticky=(W,E))
omllb_entry = ttk.Entry(mainframe,width=7,textvariable=omllb,name="omllb_entry")
omllb_entry.grid(column=5,row=5,sticky=(W,E))

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=5,sticky=(W,E))
omlub_entry = ttk.Entry(mainframe,width=7,textvariable=omlub,name="omlub_entry")
omlub_entry.grid(column=7,row=5,sticky=(W,E))

ttk.Label(mainframe,text="Interval").grid(column=8,row=5,sticky=(W,E))
omlint_entry = ttk.Entry(mainframe,width=7,textvariable=omlint,name="omlint_entry")
omlint_entry.grid(column=9,row=5,sticky=(W,E))

###

omuel = StringVar()
omulb = StringVar()
omuub = StringVar()
omuint = StringVar()

ttk.Label(mainframe,text=u'\u03A9').grid(column=2,row=6,sticky=(W,E))

omu_entry = ttk.Entry(mainframe,width=7,textvariable=omuel,name="omu_entry")
omu_entry.grid(column=3,row=6,sticky=(W,E))

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=6,sticky=(W,E))
omulb_entry = ttk.Entry(mainframe,width=7,textvariable=omulb,name="omulb_entry")
omulb_entry.grid(column=5,row=6,sticky=(W,E))

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=6,sticky=(W,E))
omuub_entry = ttk.Entry(mainframe,width=7,textvariable=omuub,name="omuub_entry")
omuub_entry.grid(column=7,row=6,sticky=(W,E))

ttk.Label(mainframe,text="Interval").grid(column=8,row=6,sticky=(W,E))
omuint_entry = ttk.Entry(mainframe,width=7,textvariable=omuint,name="omuint_entry")
omuint_entry.grid(column=9,row=6,sticky=(W,E))

###

nuel = StringVar()
nulb = StringVar()
nuub = StringVar()
nuint = StringVar()

ttk.Label(mainframe,text=u'\u03BD').grid(column=2,row=7,sticky=(W,E))

nu_entry = ttk.Entry(mainframe,width=7,textvariable=nuel,name="nu_entry")
nu_entry.grid(column=3,row=7,sticky=(W,E))

ttk.Label(mainframe,text="Lower bound").grid(column=4,row=7,sticky=(W,E))
nulb_entry = ttk.Entry(mainframe,width=7,textvariable=nulb,name="nulb_entry")
nulb_entry.grid(column=5,row=7,sticky=(W,E))

ttk.Label(mainframe,text="Upper bound").grid(column=6,row=7,sticky=(W,E))
nuub_entry = ttk.Entry(mainframe,width=7,textvariable=nuub,name="nuub_entry")
nuub_entry.grid(column=7,row=7,sticky=(W,E))

ttk.Label(mainframe,text="Interval").grid(column=8,row=7,sticky=(W,E))
nuint_entry = ttk.Entry(mainframe,width=7,textvariable=nuint,name="nuint_entry")
nuint_entry.grid(column=9,row=7,sticky=(W,E))

###

masssat = StringVar()
ttk.Label(mainframe,text="Satellite Mass (kg)").grid(column = 2,row = 8, sticky=(W,E))
mass_entry = ttk.Entry(mainframe,width=7,textvariable=masssat,name="masssat")
mass_entry.grid(column=3,row=8,sticky=(W,E))

scenname = StringVar()
ttk.Label(mainframe,text="Scenario Name").grid(column=2,row=9,sticky=(W,E))
scenname_entry = ttk.Entry(mainframe,width=14,textvariable=scenname,name="scenname")
scenname_entry.grid(column=3,row=9,sticky=(W,E))

#ttk.Button(mainframe,text="Assign Conditions",command=assign).grid(column=2,row=10,sticky=W)
ttk.Button(mainframe,text="Run",command=run).grid(column=2,row=10,sticky=W)

for child in mainframe.winfo_children(): #sets padding properties for all the widgets in mainframe
    child.grid_configure(padx=5,pady=5)
rp_entry.focus() #puts cursor in entry when opened
root.bind("<Return>",run) #sets enter key to behave like run button

root.mainloop() #makes it do the things